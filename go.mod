module gitlab.com/thefinn93/stable-diffusion-signal-bot

go 1.19

require (
	github.com/deepmap/oapi-codegen v1.12.4
	github.com/sirupsen/logrus v1.9.0
	gitlab.com/signald/signald-go v0.5.2
)

require (
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	golang.org/x/sys v0.2.0 // indirect
)
