package config

import (
	"encoding/json"
	"os"

	log "github.com/sirupsen/logrus"

	"gitlab.com/thefinn93/stable-diffusion-signal-bot/stablediffusion"
)

type Configuration struct {
	SignalAccount      string                                                       `json:"signal_account"`
	Debug              bool                                                         `json:"debug"`
	StableDiffusionURL string                                                       `json:"stable_diffusion_url"`
	SDOptions          stablediffusion.Text2imgapiSdapiV1Txt2imgPostJSONRequestBody `json:"sd_options"`
	ImgSDOptions       stablediffusion.StableDiffusionProcessingImg2Img             `json:"img_sd_options"`
}

var (
	C = Configuration{}
)

// Load the config from a file on disk
func Load() error {
	f, err := os.Open("stable-diffusion-bot.json")
	if err != nil {
		return err
	}
	defer f.Close()

	err = json.NewDecoder(f).Decode(&C)
	if err != nil {
		return err
	}

	if C.Debug {
		log.SetLevel(log.DebugLevel)
		log.Debug("debug logging enabled")
	} else {
		log.SetLevel(log.InfoLevel)
	}

	return nil
}
