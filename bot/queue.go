package bot

import (
	"fmt"
	"runtime/debug"
	"time"

	log "github.com/sirupsen/logrus"
)

var (
	queue             = make(chan job, 100)
	inprogress        = false
	shutdown          = make(chan struct{})
	shutdownRequested = false
)

// RunQueue processes jobs from the queue in order
func RunQueue() {
	go func() {
		for {
			if len(queue) > 0 {
				log.Info("current queue size: ", len(queue))
			}
			time.Sleep(time.Minute * 5)
		}
	}()

	for {
		log.Info("waiting for next job in queue")
		j := <-queue
		inprogress = true
		defer func() {
			inprogress = false
		}()

		log.Debug("got job, working on it")
		doJob(j)

		if shutdownRequested {
			if len(queue) > 0 {
				log.Info(len(queue), " items remaining in queue, will exit when queue is empty")
			} else {
				shutdown <- struct{}{}
				return
			}
		}
	}
}

func doJob(j job) {
	defer func() {
		r := recover()
		if r == nil {
			return
		}

		log.Error("job crashed: ", r)
		debug.PrintStack()

		j.error(fmt.Errorf("unexpected error: %v", r))
	}()

	if err := j.Run(); err != nil {
		log.Warn("error running job: ", err)
		j.error(err)
	}
}

func addToQueue(j job) {
	if len(queue) > 0 || inprogress {
		log.Debug("adding ⏳ to queued job because queue size ", len(queue), " inprogress ", inprogress)
		if err := j.react("⏳"); err != nil {
			log.Error("error")
		}
	}

	queue <- j

	log.Infof("job added to queue (%d)", len(queue))
}
