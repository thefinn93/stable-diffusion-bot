package bot

import (
	"bytes"
	"context"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/thefinn93/stable-diffusion-signal-bot/config"
	"gitlab.com/thefinn93/stable-diffusion-signal-bot/stablediffusion"
)

var (
	batchSize = 4
	width     = 512
	height    = 512

	charset     = []rune("abcdefghijklmnopqrstuvwxyz")
	charsetSize = len(charset)
)

type Result struct {
	Images     []string
	SessionDir string
}

// func init() {
// 	var err error
// 	log.Debug("creating client for stable diffusion ", config.C.StableDiffusionURL)
// 	sd, err = NewClientWithResponses(config.C.StableDiffusionURL, WithHTTPClient(&http.Client{Timeout: 5 * time.Minute}))
// 	if err != nil {
// 		log.Fatal("error initializing Stable Diffusion client: %V", err)
// 	}
// }

func txt2img(prompt string) (Result, error) {
	result := Result{}

	sd, err := stablediffusion.NewClientWithResponses(config.C.StableDiffusionURL, stablediffusion.WithHTTPClient(&http.Client{Timeout: 5 * time.Minute}))
	if err != nil {
		return result, fmt.Errorf("error creating client: %v", err)
	}

	requestBody := config.C.SDOptions
	requestBody.BatchSize = &batchSize
	requestBody.Prompt = &prompt
	requestBody.Width = &width
	requestBody.Height = &height

	start := time.Now()
	resp, err := sd.Text2imgapiSdapiV1Txt2imgPostWithResponse(context.Background(), requestBody)
	if err != nil {
		return result, fmt.Errorf("error making request: %v", err)
	}

	log.Debug("request to stable diffusion took ", time.Since(start).Round(time.Millisecond))

	if resp.JSON422 != nil {
		if resp.JSON422.Detail == nil {
			return result, errors.New("error processing response")
		}

		errors := []string{}
		for _, validationError := range *resp.JSON422.Detail {
			log.Warnf("Validation error: %s: %s: %+v", validationError.Type, validationError.Msg, validationError.Loc)
			errors = append(errors, fmt.Sprintf("%s: %s", validationError.Type, validationError.Msg))
		}

		return result, fmt.Errorf("validation error(s):\n * %s", strings.Join(errors, "* "))
	}

	if resp.JSON200 == nil {
		return result, fmt.Errorf("%s: %s", resp.HTTPResponse.Status, string(resp.Body))
	}

	dir, err := ioutil.TempDir(os.TempDir(), "stable-diffusion-bot-")
	if err != nil {
		return result, err
	}
	result.SessionDir = dir
	os.Chmod(dir, 0755)

	for i, img := range *resp.JSON200.Images {
		filename := filepath.Join(dir, fmt.Sprintf("%d.jpg", i))

		decoder := base64.NewDecoder(base64.StdEncoding, bytes.NewBufferString(img))

		f, err := os.Create(filename)
		if err != nil {
			return result, fmt.Errorf("error creating %s: %V", filename, err)
		}
		defer f.Close()

		_, err = io.Copy(f, decoder)
		if err != nil {
			return result, fmt.Errorf("error writing file %s: %V", filename, err)
		}

		result.Images = append(result.Images, filename)
	}

	log.Debug("request complete")

	return result, nil
}

func img2img(attachments []string, prompt string) (Result, error) {
	result := Result{}

	sd, err := stablediffusion.NewClientWithResponses(config.C.StableDiffusionURL, stablediffusion.WithHTTPClient(&http.Client{Timeout: 5 * time.Minute}))
	if err != nil {
		return result, fmt.Errorf("error creating client: %v", err)
	}

	a := []interface{}{}
	for _, attachment := range attachments {
		a = append(a, attachment)
	}

	requestBody := config.C.ImgSDOptions
	requestBody.BatchSize = &batchSize
	requestBody.InitImages = &a
	requestBody.Prompt = &prompt
	requestBody.Width = &width
	requestBody.Height = &height

	start := time.Now()
	resp, err := sd.Img2imgapiSdapiV1Img2imgPostWithResponse(context.Background(), requestBody)
	if err != nil {
		return result, fmt.Errorf("error making request: %v", err)
	}

	log.Debug("request to stable diffusion took ", time.Since(start).Round(time.Millisecond))

	if resp.JSON422 != nil {
		if resp.JSON422.Detail == nil {
			return result, errors.New("error processing response")
		}

		errors := []string{}
		for _, validationError := range *resp.JSON422.Detail {
			log.Warnf("Validation error: %s: %s: %+v", validationError.Type, validationError.Msg, validationError.Loc)
			errors = append(errors, fmt.Sprintf("%s: %s", validationError.Type, validationError.Msg))
		}

		return result, fmt.Errorf("validation error(s):\n * %s", strings.Join(errors, "* "))
	}

	if resp.JSON200 == nil {
		return result, fmt.Errorf("%s: %s", resp.HTTPResponse.Status, string(resp.Body))
	}

	dir, err := ioutil.TempDir(os.TempDir(), "stable-diffusion-bot-")
	if err != nil {
		return result, err
	}
	result.SessionDir = dir
	os.Chmod(dir, 0755)

	for i, img := range *resp.JSON200.Images {
		filename := filepath.Join(dir, fmt.Sprintf("%d.jpg", i))

		decoder := base64.NewDecoder(base64.StdEncoding, bytes.NewBufferString(img))

		f, err := os.Create(filename)
		if err != nil {
			return result, fmt.Errorf("error creating %s: %V", filename, err)
		}
		defer f.Close()

		_, err = io.Copy(f, decoder)
		if err != nil {
			return result, fmt.Errorf("error writing file %s: %V", filename, err)
		}

		result.Images = append(result.Images, filename)
	}

	log.Debug("request complete")

	return result, nil
}

func generateSession() (id string) {
	for len(id) < 10 {
		id = id + string(charset[rand.Int()%charsetSize])
	}
	return id
}
