package bot

import (
	"encoding/base64"
	"encoding/json"
	"io"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"gitlab.com/signald/signald-go/signald"
	client_protocol "gitlab.com/signald/signald-go/signald/client-protocol"
	"gitlab.com/signald/signald-go/signald/client-protocol/v1"

	"gitlab.com/thefinn93/stable-diffusion-signal-bot/config"
)

var (
	signaldConn = &signald.Signald{}
)

// SignalListener listens for signald messages
func SignalListener() {
	if err := signaldConn.Connect(); err != nil {
		log.Fatal("failed to connect to signald: ", err)
	}

	incoming := make(chan client_protocol.BasicResponse)
	go signaldConn.Listen(incoming)

	subscribeReq := v1.SubscribeRequest{Account: config.C.SignalAccount}
	if err := subscribeReq.Submit(signaldConn); err != nil {
		log.Fatal("error connecting to signal account: ", err)
	}

	log.Debug("waiting for incoming messages")

	for msg := range incoming {
		if msg.Type != "IncomingMessage" {
			continue
		}

		var data v1.IncomingMessage
		if err := json.Unmarshal(msg.Data, &data); err != nil {
			log.Error("error parsing message from signald: ", err)
			continue
		}

		if data.DataMessage == nil {
			continue
		}

		if len(data.DataMessage.Body) == 0 && len(data.DataMessage.Attachments) == 0 {
			continue
		}

		attachments := []string{}

		for _, attachment := range data.DataMessage.Attachments {
			f, err := os.Open(attachment.StoredFilename)
			if err != nil {
				log.Warn("error opening attachment file ", attachment, ": ", err)
				continue
			}
			defer f.Close()

			b, err := io.ReadAll(f)
			if err != nil {
				log.Warn("error reading attachment file ", attachment, ": ", err)
				continue
			}

			attachments = append(attachments, base64.StdEncoding.EncodeToString(b))
		}

		j := job{
			Source:      *data.Source,
			Timestamp:   data.DataMessage.Timestamp,
			Body:        data.DataMessage.Body,
			Attachments: attachments,
		}

		if data.DataMessage.GroupV2 != nil {
			mentioned := false
			for _, m := range data.DataMessage.Mentions {
				if m.UUID == config.C.SignalAccount {
					mentioned = true
					break
				}
			}
			if !mentioned {
				continue // ignore group messages if we weren't @'d
			}

			j.Group = data.DataMessage.GroupV2.ID

			// if mention replies to another message and has a very small body, do the message it's replying to instead
			if data.DataMessage.Quote != nil && len(strings.TrimSpace(data.DataMessage.Body)) == 3 {
				j.Source = *data.DataMessage.Quote.Author
				j.Timestamp = data.DataMessage.Quote.ID
				j.Body = data.DataMessage.Quote.Text
			}
		}

		go addToQueue(j)
	}
}

// Shutdown the message listener, block until the queue is empty, then return
func Shutdown() error {
	log.Info("shutdown requested, unsubscribing from incoming messages, messages sent after this will be processed when this program is restarted")

	// shutdown the signal listener
	req := v1.UnsubscribeRequest{Account: config.C.SignalAccount}
	if err := req.Submit(signaldConn); err != nil {
		return err
	}

	// indicate to the job runner that a shutdown has been requested
	shutdownRequested = true
	if len(queue) > 0 || inprogress {
		log.Debug(len(queue), " + current item will be processed, then program will exit")

		// job runner will push a message to the channel
		<-shutdown
	}

	return nil
}
