package bot

import (
	"context"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/signald/signald-go/signald/client-protocol/v1"

	"gitlab.com/thefinn93/stable-diffusion-signal-bot/config"
)

type job struct {
	Source      v1.JsonAddress
	Timestamp   int64
	Body        string
	Group       string
	Attachments []string
	Context     context.Context

	lastReact string
}

func (j *job) Run() error {
	j.react("🤔")

	var result Result
	var err error
	if len(j.Attachments) > 0 {
		result, err = img2img(j.Attachments, j.Body)
	} else {
		result, err = txt2img(j.Body)
	}
	if err != nil {
		return err
	}

	defer func() {
		log.Debug("deleting temp folder ", result.SessionDir)
		err := os.RemoveAll(result.SessionDir)
		if err != nil {
			log.Error("error cleaning up temp dir ", result.SessionDir, ": ", err)
		}
	}()

	log.Debug("got results from stable diffusion, sending to Signal")

	resp := v1.SendRequest{}
	for _, filename := range result.Images {
		resp.Attachments = append(resp.Attachments, &v1.JsonAttachment{Filename: filename})
	}

	if err := j.reply(resp); err != nil {
		return err
	}

	log.Debug("results uploaded, reacting to original request")
	j.react("✨")

	log.Debug("job complete")
	return nil
}

func (j *job) reply(s v1.SendRequest) error {
	s.Username = config.C.SignalAccount
	s.Quote = &v1.JsonQuote{
		Author: &j.Source,
		ID:     j.Timestamp,
		Text:   j.Body,
	}

	if j.Group != "" {
		s.RecipientGroupID = j.Group
	} else {
		s.RecipientAddress = &j.Source
	}

	_, err := s.Submit(signaldConn)
	return err
}

func (j *job) react(emoji string) error {
	remove := emoji == ""
	if remove {
		emoji = j.lastReact
		j.lastReact = ""
	} else {
		j.lastReact = emoji
	}

	reactReq := v1.ReactRequest{
		Username: config.C.SignalAccount,
		Reaction: &v1.JsonReaction{
			Emoji:               emoji,
			TargetAuthor:        &j.Source,
			TargetSentTimestamp: j.Timestamp,
			Remove:              remove,
		},
	}

	if j.Group != "" {
		reactReq.RecipientGroupID = j.Group
	} else {
		reactReq.RecipientAddress = &j.Source
	}

	_, err := reactReq.Submit(signaldConn)
	if err != nil {
		return err
	}

	return nil
}

func (j *job) error(err error) {
	j.react("⚠️")
	_ = j.reply(v1.SendRequest{MessageBody: fmt.Sprintf("⚠️ %s", err.Error())})
}
