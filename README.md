# Stable Diffusion Bot
*a signal bot to use stable diffusion web ui*

Needs [signald](https://signald.org). Must have an account in signald. Put config in `stable-diffusion-bot.json` in the working directory the program gets run in. Sample:

```
{
    "signal_account": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    "stable_diffusion_url": "http://my.stable-diffusion.server:9999"
}
```

`signal_account` must be a UUID. All options in `config/config.go`.
