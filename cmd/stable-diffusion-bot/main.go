package main

import (
	"fmt"
	"runtime"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/thefinn93/stable-diffusion-signal-bot/bot"
	"gitlab.com/thefinn93/stable-diffusion-signal-bot/config"
)

var (
	repoName = "stable-diffusion-signal-bot"
	// signals  = make(chan os.Signal, 1)
)

func main() {
	log.SetReportCaller(true)
	log.SetFormatter(&log.TextFormatter{
		FullTimestamp:             true,
		EnvironmentOverrideColors: true,
		CallerPrettyfier: func(f *runtime.Frame) (string, string) {
			filename := f.File

			filenameParts := strings.SplitN(f.File, repoName+"/", 2)
			if len(filenameParts) > 1 {
				filename = filenameParts[1]
			} else {
				filenameParts = strings.Split(f.File, "/")
				filename = filenameParts[len(filenameParts)-1]
			}
			filename = fmt.Sprintf(" %s:%d", filename, f.Line)
			s := strings.Split(f.Function, ".")
			funcname := fmt.Sprintf("%s()", s[len(s)-1])
			return funcname, filename
		},
	})

	if err := config.Load(); err != nil {
		log.Fatal("error loading config: ", err)
	}

	// signal.Notify(signals, syscall.SIGINT)

	go bot.RunQueue()
	bot.SignalListener()

	// for {
	// 	signal := <-signals
	// 	log.Debug("received signal", signal)
	// 	switch signal {
	// 	case syscall.SIGINT:
	// 		err := bot.Shutdown()
	// 		if err != nil {
	// 			log.Error("error shutting down bot: ", err)
	// 		}
	// 		return
	// 	}
	// }
}
